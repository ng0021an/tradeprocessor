Feature: Aggregated Trades Filter
As a user
I want to filter out invalid trade records
So that the aggregated trades are valid

Scenario: Filter out data with extra scenario
    Given trades with
    |date|desk|product|currency|instrument|underlyer|scenario|pnl|
    |20160101|desk1|product1|EUR|1234444|SCB|1|10|
    |20160101|desk1|product1|EUR|1234444|SCB|2|20|
    |20160101|desk1|product1|EUR|1234444|SCB|3|30|
    |20160101|desk1|product1|EUR|1234444|SCB|4|999|
    |20160101|desk1|product1|EUR|1234444|SCB|5|-1E+29|
    |20160101|desk1|product1|EUR|1234444|SCB|6|6.66667E+60|
    |20160101|desk1|product1|EUR|1234444|SCB|7|999|
    |20160101|desk1|product1|EUR|1234444|SCB|8|589|
    |20160101|desk1|product1|EUR|1234444|SCB|9|4355|
    |20160101|desk1|product1|EUR|1234444|SCB|10|97|
    |20160101|desk1|product1|EUR|1234444|SCB|11|20|
    |20160101|desk1|product1|EUR|1234444|SCB|12|30|
    |20160101|desk1|product1|EUR|1234444|SCB|13|999|
    |20160101|desk1|product1|EUR|1234444|SCB|14|-1E+29|
    |20160101|desk1|product1|EUR|1234444|SCB|15|999|
    |20160101|desk1|product1|EUR|1234444|SCB|16|589|
    |20160101|desk1|product1|EUR|1234444|SCB|17|4355|
    |20160101|desk1|product1|EUR|1234444|SCB|18|97|
    When I invoke filter
    Then the filter should return false

Scenario: Filter out data with missing scenario
    Given trades with
    |date|desk|product|currency|instrument|underlyer|scenario|pnl|
    |20160101|desk2|product1|EUR|1234444|SCB|1|10|
    When I invoke filter
    Then the filter should return false

Scenario: Filter out data with duplicate scenario
    Given trades with
    |date|desk|product|currency|instrument|underlyer|scenario|pnl|
    |20160101|desk1|product1|EUR|1234444|SCB|1|10|
    |20160101|desk1|product1|EUR|1234444|SCB|2|20|
    |20160101|desk1|product1|EUR|1234444|SCB|3|30|
    |20160101|desk1|product1|EUR|1234444|SCB|4|999|
    |20160101|desk1|product1|EUR|1234444|SCB|5|-1E+29|
    |20160101|desk1|product1|EUR|1234444|SCB|6|6.66667E+60|
    |20160101|desk1|product1|EUR|1234444|SCB|7|999|
    |20160101|desk1|product1|EUR|1234444|SCB|2|589|
    |20160101|desk1|product1|EUR|1234444|SCB|9|4355|
    |20160101|desk1|product1|EUR|1234444|SCB|10|97|
    When I invoke filter
    Then the filter should return false

Scenario: Filter out data with invalid date
    Given trades with
    |date|desk|product|currency|instrument|underlyer|scenario|pnl|
    |2016/01/01|desk1|product1|EUR|1234444|SCB|1|10|
    |2016/01/01|desk1|product1|EUR|1234444|SCB|2|20|
    |2016/01/01|desk1|product1|EUR|1234444|SCB|3|30|
    |2016/01/01|desk1|product1|EUR|1234444|SCB|4|999|
    |2016/01/01|desk1|product1|EUR|1234444|SCB|5|-1E+29|
    |2016/01/01|desk1|product1|EUR|1234444|SCB|6|6.66667E+60|
    |2016/01/01|desk1|product1|EUR|1234444|SCB|7|999|
    |2016/01/01|desk1|product1|EUR|1234444|SCB|8|589|
    |2016/01/01|desk1|product1|EUR|1234444|SCB|9|4355|
    |2016/01/01|desk1|product1|EUR|1234444|SCB|10|97|
    When I invoke filter
    Then the filter should return false

Scenario: Filter out data with invalid currency
    Given trades with
    |date|desk|product|currency|instrument|underlyer|scenario|pnl|
    |20160101|desk1|product1|EURS|1234444|SCB|1|10|
    |20160101|desk1|product1|EURS|1234444|SCB|2|20|
    |20160101|desk1|product1|EURS|1234444|SCB|3|30|
    |20160101|desk1|product1|EURS|1234444|SCB|4|999|
    |20160101|desk1|product1|EURS|1234444|SCB|5|-1E+29|
    |20160101|desk1|product1|EURS|1234444|SCB|6|6.66667E+60|
    |20160101|desk1|product1|EURS|1234444|SCB|7|999|
    |20160101|desk1|product1|EURS|1234444|SCB|8|589|
    |20160101|desk1|product1|EURS|1234444|SCB|9|4355|
    |20160101|desk1|product1|EURS|1234444|SCB|10|97|
    When I invoke filter
    Then the filter should return false

Scenario: Filter out data with invalid scenario
    Given trades with
    |date|desk|product|currency|instrument|underlyer|scenario|pnl|
    |20160101|desk1|product1|EUR|1234444|SCB|1|10|
    |20160101|desk1|product1|EUR|1234444|SCB|2|20|
    |20160101|desk1|product1|EUR|1234444|SCB|AB|30|
    |20160101|desk1|product1|EUR|1234444|SCB|4|999|
    |20160101|desk1|product1|EUR|1234444|SCB|5|-1E+29|
    |20160101|desk1|product1|EUR|1234444|SCB|6|6.66667E+60|
    |20160101|desk1|product1|EUR|1234444|SCB|7|999|
    |20160101|desk1|product1|EUR|1234444|SCB|8|589|
    |20160101|desk1|product1|EUR|1234444|SCB|9|4355|
    |20160101|desk1|product1|EUR|1234444|SCB|10|97|
    When I invoke filter
    Then the filter should return false

Scenario: Filter out data with invalid pnl
    Given trades with
    |date|desk|product|currency|instrument|underlyer|scenario|pnl|
    |20160101|desk1|product1|EUR|1234444|SCB|1|10|
    |20160101|desk1|product1|EUR|1234444|SCB|2|20|
    |20160101|desk1|product1|EUR|1234444|SCB|3|30|
    |20160101|desk1|product1|EUR|1234444|SCB|4|999|
    |20160101|desk1|product1|EUR|1234444|SCB|5|-1E+29|
    |20160101|desk1|product1|EUR|1234444|SCB|6|6.66667E+60|
    |20160101|desk1|product1|EUR|1234444|SCB|7|9 9 9|
    |20160101|desk1|product1|EUR|1234444|SCB|8|589|
    |20160101|desk1|product1|EUR|1234444|SCB|9|4355|
    |20160101|desk1|product1|EUR|1234444|SCB|10|97|
    When I invoke filter
    Then the filter should return false

Scenario: Accept valid data
    Given trades with
    |date|desk|product|currency|instrument|underlyer|scenario|pnl|
    |20160101|desk1|product1|EUR|1234444|SCB|1|10|
    |20160101|desk1|product1|EUR|1234444|SCB|2|20|
    |20160101|desk1|product1|EUR|1234444|SCB|3|30|
    |20160101|desk1|product1|EUR|1234444|SCB|4|999|
    |20160101|desk1|product1|EUR|1234444|SCB|5|-1E+29|
    |20160101|desk1|product1|EUR|1234444|SCB|6|6.66667E+60|
    |20160101|desk1|product1|EUR|1234444|SCB|7|999|
    |20160101|desk1|product1|EUR|1234444|SCB|8|589|
    |20160101|desk1|product1|EUR|1234444|SCB|9|4355|
    |20160101|desk1|product1|EUR|1234444|SCB|10|97|
    When I invoke filter
    Then the filter should return true

