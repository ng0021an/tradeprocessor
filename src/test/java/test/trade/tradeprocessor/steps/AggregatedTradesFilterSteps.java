package test.trade.tradeprocessor.steps;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import test.trade.tradeprocessor.data.Trade;
import test.trade.tradeprocessor.data.TradeKey;
import test.trade.tradeprocessor.module.TradeProcessorModule;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class AggregatedTradesFilterSteps {

    private Predicate<Map.Entry<TradeKey, List<Trade>>> aggregatedTradesFilter;
    private Map.Entry<TradeKey, List<Trade>> tradeKeyListEntry;
    private boolean result;

    @Before
    public void setUp() {
        Injector injector = Guice.createInjector(new TradeProcessorModule());
        this.aggregatedTradesFilter = injector.getInstance(Key.get(Predicate.class, TradeProcessorModule.AggregationFilter.class));
    }

    @Given("^trades with$")
    public void trade_with_fields(DataTable table) throws Throwable {
        List<Map<String,String>> data = table.asMaps(String.class,String.class);
        this.tradeKeyListEntry = data.stream().map(m -> new Trade(
                new TradeKey(Optional.of(m.get("date")),
                        Optional.of(m.get("desk")),
                        Optional.of(m.get("product")),
                        Optional.of(m.get("currency")),
                        Optional.of(m.get("instrument")),
                        Optional.of(m.get("underlyer"))),
                Optional.of(m.get("scenario")),
                Optional.of(m.get("pnl"))
            )
        ).collect(Collectors.groupingBy(Trade::getTradeKey)).entrySet().iterator().next();
    }

    @When("^I invoke filter$")
    public void i_invoke_filters() throws Throwable {
        this.result = this.aggregatedTradesFilter.test(this.tradeKeyListEntry);
    }

    @Then("^the filter should return (.*)$")
    public void the_filter_should_return(boolean result) throws Throwable {
        System.out.println(this.result);
        assertEquals(this.result, result);
    }

}
