package test.trade.tradeprocessor;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import test.trade.tradeprocessor.configuration.Configuration;


/**
 * A simple facade for the trade processing unit
 */
@Singleton
public class TradeProcessingUnit {

    private final TradeProcessor tradeProcessor;
    private final Configuration configuration;

    @Inject
    public TradeProcessingUnit(TradeProcessor tradeProcessor, Configuration configuration) {
        this.tradeProcessor = tradeProcessor;
        this.configuration = configuration;
    }

    public void processTrade(String fileName) {
        this.tradeProcessor.processTradesFromFile(fileName);
    }

    public void setConfig(String key, Object value) {
        this.configuration.setConfig(key, value);
    }

    public Object getConfig(String key) {
        return this.configuration.getConfig(key);
    }

}
