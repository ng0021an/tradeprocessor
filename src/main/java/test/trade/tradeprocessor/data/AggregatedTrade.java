package test.trade.tradeprocessor.data;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AggregatedTrade {
    private final TradeKey tradeKey;
    private final BigDecimal pnl;

    @Override
    public String toString() {
        return String.format("%s,%s", tradeKey.toString(), pnl.toString());
    }
}
