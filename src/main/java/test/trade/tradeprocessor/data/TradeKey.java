package test.trade.tradeprocessor.data;

import lombok.Data;

import java.util.Optional;

@Data
public class TradeKey {
    private final Optional<String> date;
    private final Optional<String> desk;
    private final Optional<String> product;
    private final Optional<String> currency;
    private final Optional<String> instrument;
    private final Optional<String> underlyer;

    @Override
    public String toString() {
        return String.format("%s,%s,%s,%s,%s,%s", date.orElse(""), desk.orElse(""), product.orElse(""),
                currency.orElse(""), instrument.orElse(""), underlyer.orElse(""));
    }
}
