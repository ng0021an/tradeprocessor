package test.trade.tradeprocessor.data;

import lombok.Data;
import lombok.Getter;

import java.util.Optional;

@Data
public class Trade {
    @Getter private final TradeKey tradeKey;
    @Getter private final Optional<String> scenario;
    @Getter private final Optional<String> pnl;

    @Override
    public String toString() {
        return String.format("%s,%s,%s", tradeKey.toString(), scenario.orElse(""), pnl.orElse(""));
    }
}
