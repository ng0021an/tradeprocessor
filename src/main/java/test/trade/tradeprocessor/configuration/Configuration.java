package test.trade.tradeprocessor.configuration;

import com.google.common.collect.Maps;
import com.google.inject.Singleton;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;

import java.util.Map;

/**
 * @TODO: Provide the capability to load from config file
 */

@Singleton
@Getter @Setter
@Log4j
public class Configuration {

    public static final String NUM_SCENARIO = "numScenario";
    public static final String EXPORT_QUEUE_SIZE = "numScenario";
    public static final String ERROR_FILE = "errorFile";
    public static final String AGGREGATION_FILE = "aggregationFile";

    private final Map<String, Object> configs = Maps.newHashMap();

    public void setConfig(String key, Object value) {
        configs.put(key, value);
    }

    public Object getConfig(String key) {
        return configs.get(key);
    }

    public Object getConfig(String key, Object defaultValue) {
        Object value = configs.get(key);
        return value == null ? defaultValue : value;
    }

    public Integer getConfigInt(String key, Integer defaultValue) {
        try {
            return (Integer) getConfig(key, defaultValue);
        }
        catch (Exception e) {
            log.error("Error getting config as int:" + key, e);
            return defaultValue;
        }
    }

    public String getConfigString(String key, String defaultValue) {
        try {
            return (String) getConfig(key, defaultValue);
        }
        catch (Exception e) {
            log.error("Error getting config as string:" + key, e);
            return defaultValue;
        }
    }
}
