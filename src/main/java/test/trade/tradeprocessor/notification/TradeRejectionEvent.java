package test.trade.tradeprocessor.notification;

import lombok.Data;
import test.trade.tradeprocessor.data.Trade;

import java.util.List;

@Data
public class TradeRejectionEvent {
    private final List<Trade> trades;
}
