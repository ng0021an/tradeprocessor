package test.trade.tradeprocessor.notification;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import test.trade.tradeprocessor.handler.TradeAggregationExporter;
import test.trade.tradeprocessor.handler.TradeRejectionExporter;
import test.trade.tradeprocessor.module.TradeProcessorModule;

@Singleton
public class NotificationCentre {

    private final TradeAggregationExporter tradeAggregationExporter;
    private final TradeRejectionExporter tradeRejectionExporter;

    @Inject
    public NotificationCentre(@TradeProcessorModule.Service EventBus eventBus, TradeAggregationExporter tradeAggregationExporter, TradeRejectionExporter tradeRejectionExporter) {
        this.tradeAggregationExporter = tradeAggregationExporter;
        this.tradeRejectionExporter = tradeRejectionExporter;
        eventBus.register(this);
    }

    @Subscribe
    public void onTradeAggregated(TradeAggregationEvent event) {
        tradeAggregationExporter.exportTrade(event.getAggregatedTrade());
    }

    @Subscribe
    public void onTradeRejection(TradeRejectionEvent event) {
        tradeRejectionExporter.exportTrades(event.getTrades());
    }

}
