package test.trade.tradeprocessor.notification;

import lombok.Data;
import test.trade.tradeprocessor.data.AggregatedTrade;

@Data
public class TradeAggregationEvent {
    private final AggregatedTrade aggregatedTrade;
}
