package test.trade.tradeprocessor.validator;

import com.google.inject.Singleton;
import lombok.extern.log4j.Log4j;
import test.trade.tradeprocessor.data.Trade;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.function.Predicate;

@Singleton
@Log4j
public class DateValidator implements Predicate<Trade> {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");

    @Override
    public boolean test(Trade trade) {
        try {
            return LocalDate.parse(trade.getTradeKey().getDate().get(), formatter) != null;
        }
        catch (Exception e) {
            log.error("Error parsing date for trade:" + trade, e);
            return false;
        }
    }
}
