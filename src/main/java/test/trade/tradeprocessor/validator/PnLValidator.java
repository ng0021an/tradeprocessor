package test.trade.tradeprocessor.validator;

import com.google.inject.Singleton;
import lombok.extern.log4j.Log4j;
import test.trade.tradeprocessor.data.Trade;

import java.math.BigDecimal;
import java.util.function.Predicate;

@Singleton
@Log4j
public class PnLValidator implements Predicate<Trade> {

    @Override
    public boolean test(Trade trade) {
        try {
            BigDecimal pnl = new BigDecimal(trade.getPnl().get());
            return pnl != null;
        }
        catch (Exception e) {
            log.error("Error parsing PnL for trade:" + trade, e);
            return false;
        }
    }
}