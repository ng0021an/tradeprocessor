package test.trade.tradeprocessor.validator;

import com.google.inject.Singleton;
import lombok.extern.log4j.Log4j;
import test.trade.tradeprocessor.data.Trade;

import java.util.function.Predicate;

@Singleton
@Log4j
public class ScenarioValidator implements Predicate<Trade> {

    @Override
    public boolean test(Trade trade) {
        try {
            Long.parseLong(trade.getScenario().get());
            return true;
        }
        catch (Exception e) {
            log.error("Error parsing scenario for trade:" + trade, e);
            return false;
        }
    }
}