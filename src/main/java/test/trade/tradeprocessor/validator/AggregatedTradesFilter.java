package test.trade.tradeprocessor.validator;

import com.google.common.collect.Lists;
import com.google.common.eventbus.EventBus;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import test.trade.tradeprocessor.data.Trade;
import test.trade.tradeprocessor.data.TradeKey;
import test.trade.tradeprocessor.module.TradeProcessorModule;
import test.trade.tradeprocessor.notification.TradeRejectionEvent;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

/**
 * This class filter out invalid trade keys and also log the emerging erroneous record upon encounter
 */
@Singleton
public class AggregatedTradesFilter implements Predicate<Map.Entry<TradeKey, List<Trade>>> {

    private final AggregatedTradeValidator aggregatedTradeValidator;
    private final Predicate<Trade> tradeValidator;
    private final EventBus eventBus;

    @Inject
    public AggregatedTradesFilter(@TradeProcessorModule.TradeValidator Predicate tradeValidator, AggregatedTradeValidator aggregatedTradeValidator,
                                  @TradeProcessorModule.Service EventBus eventBus) {
        this.tradeValidator = tradeValidator;
        this.aggregatedTradeValidator = aggregatedTradeValidator;
        this.eventBus = eventBus;
    }

    @Override
    public boolean test(Map.Entry<TradeKey, List<Trade>> tradeKeyListEntry) {
        //We test for correctness of scenario first
        if (!aggregatedTradeValidator.test(tradeKeyListEntry.getValue())) {
            eventBus.post(new TradeRejectionEvent(tradeKeyListEntry.getValue()));
            return false;
        }
        //Since the expected behaviour is not specified clearly in the question. We assume that
        //if any of the record for one trade key is invalid, we will log that record into error file
        //and don't perform aggregation for the whole trade key
        return tradeKeyListEntry.getValue().stream().map(trade -> {
            if (!tradeValidator.test(trade)) {
                eventBus.post(new TradeRejectionEvent(Lists.newArrayList(trade)));
                return false;
            }
            return true;
        }).reduce(true, (b1, b2) -> b1 && b2);
    }
}
