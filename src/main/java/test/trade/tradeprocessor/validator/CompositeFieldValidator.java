package test.trade.tradeprocessor.validator;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import test.trade.tradeprocessor.data.Trade;
import test.trade.tradeprocessor.module.TradeProcessorModule;

import java.util.Set;
import java.util.function.Predicate;

/**
 * This class validate if the fields on one trade record is valid
 * We could actually put all of the validator as inline lambda inside this class
 * However if the validators rise in number and complexity, this class will become
 * too cumbersome and this may also violate single responsibility and open-closed principle
 */
@Singleton
public class CompositeFieldValidator implements Predicate<Trade> {

    private final Set<Predicate> fieldValidators;

    @Inject
    public CompositeFieldValidator(@TradeProcessorModule.FieldValidator Set<Predicate> fieldValidators) {
        this.fieldValidators = fieldValidators;
    }

    @Override
    public boolean test(Trade trade) {
        return fieldValidators.parallelStream().allMatch(validator -> validator.test(trade));
    }
}
