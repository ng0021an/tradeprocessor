package test.trade.tradeprocessor.validator;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import test.trade.tradeprocessor.configuration.Configuration;
import test.trade.tradeprocessor.data.Trade;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This class validate if the scenarios corresponding to one trade key is valid
 */
@Singleton
public class AggregatedTradeValidator implements Predicate<List<Trade>> {

    private final Set<String> expectedScenarios;
    private final int NUM_OF_SCENARIO;

    @Inject
    public AggregatedTradeValidator(Configuration configuration) {
        this.NUM_OF_SCENARIO = configuration.getConfigInt(Configuration.NUM_SCENARIO, 10);
        this.expectedScenarios = createExpectedScenario();
    }

    /**
     * It's not stated clearly in the question so we assume that the scenario will be
     * from 1 to 10 strictly
     */
    private Set<String> createExpectedScenario() {
        return IntStream.rangeClosed(1, NUM_OF_SCENARIO).mapToObj(i -> String.valueOf(i)).collect(Collectors.toSet());
    }

    @Override
    public boolean test(List<Trade> trades) {
        return trades.size() == NUM_OF_SCENARIO && collectScenariosFromTrade(trades).equals(expectedScenarios);
    }

    private Set<String> collectScenariosFromTrade(List<Trade> trades) {
         return trades.stream().map(t -> t.getScenario().orElse("")).sorted().collect(Collectors.toSet());
    }
}
