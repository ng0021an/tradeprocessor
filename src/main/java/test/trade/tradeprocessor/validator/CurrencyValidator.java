package test.trade.tradeprocessor.validator;

import com.google.inject.Singleton;
import test.trade.tradeprocessor.data.Trade;

import java.util.Optional;
import java.util.function.Predicate;

@Singleton
public class CurrencyValidator implements Predicate<Trade> {

    @Override
    public boolean test(Trade trade) {
        Optional<String> currency = trade.getTradeKey().getCurrency();
        return currency.isPresent() && currency.get().length() == 3;
    }
}