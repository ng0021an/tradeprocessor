package test.trade.tradeprocessor;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.extern.log4j.Log4j;
import test.trade.tradeprocessor.data.Trade;
import test.trade.tradeprocessor.data.TradeKey;
import test.trade.tradeprocessor.handler.TradeAggregator;
import test.trade.tradeprocessor.module.TradeProcessorModule;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This implementation assumes simple approach of publishing this as a maven module that can be
 * imported into another components that requires its functionality.
 * More advanced approach could be building this as an EJB, REST or RPC web service that can be
 * invoked from a remote client, or make this listen to messages from standard socket connections
 * or messaging queue
 *
 * Due to time constraints, this also use a simple fail-fast model where failure simply gets logged.
 * Ideally the production system should be fault-tolerant where it can recover from failure and replay the
 * uncommitted records. This would require the use of 2-phase commit and WAL which will take more time
 * to implement. We could also consider using some mature stream processing framework like Spark, Storm
 * or batch processing frameworks like Hadoop Map-Reduce to perform the task.
 */
@Singleton
@Log4j
public class TradeProcessor {

    private final Function<String, Trade> tradeParser;
    private final Predicate<Map.Entry<TradeKey, List<Trade>>> aggregationFilter;
    private final Executor executor;
    private final TradeAggregator tradeAggregator;

    @Inject
    public TradeProcessor(@TradeProcessorModule.TradeParser Function tradeParser,
                          @TradeProcessorModule.AggregationFilter Predicate aggregationFilter,
                          @TradeProcessorModule.Service Executor executor,
                          TradeAggregator tradeAggregator) {
        this.tradeParser = tradeParser;
        this.aggregationFilter = aggregationFilter;
        this.executor = executor;
        this.tradeAggregator = tradeAggregator;
    }

    /**
     * This method spawns a new thread for each file read and uses parallel stream and concurrent collector
     * for better CPU utilization
     * @param fileName
     */
    public void processTradesFromFile(String fileName) {
        log.info("Processing trade from file:" + fileName);
        executor.execute(() -> {
                    try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
                        Map<TradeKey, List<Trade>> tradesByKey = parseAndGroupTradeByKey(stream);
                        filterInvalidTradeKeysAndPerformAggregation(tradesByKey);
                    } catch (IOException ioe) {
                        log.error("Error reading file:" + fileName, ioe);
                    } catch (Exception e) {
                        log.error("Error processing trades from file:" + fileName, e);
                    }
                });
    }

    private Map<TradeKey, List<Trade>> parseAndGroupTradeByKey(Stream<String> stream) {
        return stream.skip(1).parallel()
                .map(tradeParser)
                .collect(Collectors.groupingByConcurrent(Trade::getTradeKey));
    }

    private void filterInvalidTradeKeysAndPerformAggregation(Map<TradeKey, List<Trade>> tradesByKey) {
        tradesByKey.entrySet().parallelStream().filter(aggregationFilter).forEach(tradeAggregator);
    }
}
