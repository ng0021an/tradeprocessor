package test.trade.tradeprocessor.module;

import com.google.common.eventbus.EventBus;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.multibindings.Multibinder;
import test.trade.tradeprocessor.notification.NotificationCentre;
import test.trade.tradeprocessor.validator.AggregatedTradesFilter;
import test.trade.tradeprocessor.parser.TradeRecordParser;
import test.trade.tradeprocessor.validator.CompositeFieldValidator;
import test.trade.tradeprocessor.validator.CurrencyValidator;
import test.trade.tradeprocessor.validator.DateValidator;
import test.trade.tradeprocessor.validator.PnLValidator;
import test.trade.tradeprocessor.validator.ScenarioValidator;

import javax.inject.Qualifier;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.function.Predicate;

public class TradeProcessorModule extends AbstractModule {

    private static final int NUM_THREAD = 5;

    @Override
    protected void configure() {
        Multibinder<Predicate> predicateSetBinder = Multibinder.newSetBinder(binder(), Predicate.class, FieldValidator.class);
        predicateSetBinder.addBinding().to(CurrencyValidator.class);
        predicateSetBinder.addBinding().to(DateValidator.class);
        predicateSetBinder.addBinding().to(PnLValidator.class);
        predicateSetBinder.addBinding().to(ScenarioValidator.class);
        bind(Predicate.class).annotatedWith(TradeValidator.class).to(CompositeFieldValidator.class);
        bind(Predicate.class).annotatedWith(AggregationFilter.class).to(AggregatedTradesFilter.class);
        bind(Function.class).annotatedWith(TradeParser.class).to(TradeRecordParser.class);
        bind(Function.class).annotatedWith(PnlParser.class).to(test.trade.tradeprocessor.parser.TradePnlParser.class);
        bind(NotificationCentre.class).asEagerSingleton();
    }

    @Provides @Service @Singleton
    public Executor provideServiceExecutorService() {
        return Executors.newFixedThreadPool(NUM_THREAD);
    }

    @Provides @Service @Singleton
    public EventBus provideServiceEventBus() {
        return new EventBus();
    }

    @Qualifier
    @Target({ ElementType.CONSTRUCTOR, ElementType.PARAMETER })
    @Retention(RetentionPolicy.RUNTIME)
    public @interface FieldValidator {}

    @Qualifier
    @Target({ ElementType.CONSTRUCTOR, ElementType.PARAMETER })
    @Retention(RetentionPolicy.RUNTIME)
    public @interface TradeValidator {}

    @Qualifier
    @Target({ ElementType.CONSTRUCTOR, ElementType.PARAMETER })
    @Retention(RetentionPolicy.RUNTIME)
    public @interface AggregationFilter {}

    @Qualifier
    @Target({ ElementType.CONSTRUCTOR, ElementType.PARAMETER })
    @Retention(RetentionPolicy.RUNTIME)
    public @interface TradeParser {}

    @Qualifier
    @Target({ ElementType.CONSTRUCTOR, ElementType.PARAMETER })
    @Retention(RetentionPolicy.RUNTIME)
    public @interface PnlParser {}

    @Qualifier
    @Target({ ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.METHOD })
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Service {}

}
