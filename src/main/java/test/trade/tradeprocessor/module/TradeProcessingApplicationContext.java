package test.trade.tradeprocessor.module;

import com.google.inject.Guice;
import com.google.inject.Injector;
import lombok.Getter;

public class TradeProcessingApplicationContext {

    @Getter private final TradeProcessingApplicationContext applicationContext = new TradeProcessingApplicationContext();
    @Getter private final Injector injector;

    public TradeProcessingApplicationContext() {
        this.injector = Guice.createInjector(new TradeProcessorModule());
    }

}
