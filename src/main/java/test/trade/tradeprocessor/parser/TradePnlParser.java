package test.trade.tradeprocessor.parser;

import com.google.inject.Singleton;
import lombok.extern.log4j.Log4j;
import test.trade.tradeprocessor.data.AggregatedTrade;
import test.trade.tradeprocessor.data.Trade;

import java.math.BigDecimal;
import java.util.function.Function;

@Singleton
@Log4j
public class TradePnlParser implements Function<Trade, AggregatedTrade> {

    @Override
    public AggregatedTrade apply(Trade trade) {
        try {
            return new AggregatedTrade(trade.getTradeKey(), new BigDecimal(trade.getPnl().get()));
        }
        catch (Exception e) {
            log.error("Error reading pnl for trade:" + trade, e);
            return new AggregatedTrade(trade.getTradeKey(), BigDecimal.ZERO);
        }
    }
}
