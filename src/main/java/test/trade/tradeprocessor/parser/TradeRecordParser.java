package test.trade.tradeprocessor.parser;

import com.google.inject.Singleton;
import test.trade.tradeprocessor.data.Trade;
import test.trade.tradeprocessor.data.TradeKey;

import java.util.Optional;
import java.util.function.Function;

@Singleton
public class TradeRecordParser implements Function<String, Trade> {

    @Override
    public Trade apply(String line) {
        String tokens[] = line.split(",");
        return createTrade(tokens);
    }

    private Trade createTrade(String[] tokens) {
        TradeKey tradeKey = createTradeKey(tokens);
        return new Trade(tradeKey, getField(6, tokens), getField(7, tokens));
    }

    private TradeKey createTradeKey(String[] tokens) {
        return new TradeKey(
                getField(0, tokens),
                getField(1, tokens),
                getField(2, tokens),
                getField(3, tokens),
                getField(4, tokens),
                getField(5, tokens));
    }

    private Optional<String> getField(int index, String[] tokens) {
        return tokens.length > index ? Optional.of(tokens[index]) : Optional.empty();
    }
}
