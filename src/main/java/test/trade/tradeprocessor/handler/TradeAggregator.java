package test.trade.tradeprocessor.handler;

import com.google.common.eventbus.EventBus;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import test.trade.tradeprocessor.data.AggregatedTrade;
import test.trade.tradeprocessor.data.Trade;
import test.trade.tradeprocessor.data.TradeKey;
import test.trade.tradeprocessor.module.TradeProcessorModule;
import test.trade.tradeprocessor.notification.TradeAggregationEvent;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

@Singleton
public class TradeAggregator implements Consumer<Map.Entry<TradeKey, List<Trade>>> {

    private final EventBus eventBus;
    private final Function<Trade, AggregatedTrade> pnlParser;

    @Inject
    public TradeAggregator(@TradeProcessorModule.Service EventBus eventBus,
                           @TradeProcessorModule.PnlParser Function pnlParser) {
        this.eventBus = eventBus;
        this.pnlParser = pnlParser;
    }

    @Override
    public void accept(Map.Entry<TradeKey, List<Trade>> tradeKeyListEntry) {
        AggregatedTrade aggregatedTrade = tradeKeyListEntry.getValue().stream().map(pnlParser).reduce(
                new AggregatedTrade(tradeKeyListEntry.getKey(), BigDecimal.ZERO),
                (aggTrade1, aggTrade2) -> new AggregatedTrade(tradeKeyListEntry.getKey(), aggTrade1.getPnl().add(aggTrade2.getPnl()))
        );

        eventBus.post(new TradeAggregationEvent(aggregatedTrade));
    }
}
