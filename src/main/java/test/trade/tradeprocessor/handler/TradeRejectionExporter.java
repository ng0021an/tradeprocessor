package test.trade.tradeprocessor.handler;

import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.extern.log4j.Log4j;
import test.trade.tradeprocessor.configuration.Configuration;
import test.trade.tradeprocessor.data.Trade;
import test.trade.tradeprocessor.module.TradeProcessorModule;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * We use a single thread to write to the file as using multiple thread to write to different file
 * offset generally suffer from worse IO performance due to multiple disk seek. We use a blocking
 * queue to buffer the write and the write thread will keep pulling from that queue
 */
@Singleton
@Log4j
public class TradeRejectionExporter {

    private static final int DEFAULT_QUEUE_SIZE = 1000;

    private final String DEFAULT_PATH = "error.txt";

    private final String filePath;
    private final BlockingQueue<Trade> queue;

    @Inject
    public TradeRejectionExporter(Configuration config) {
        this.filePath = config.getConfigString(Configuration.ERROR_FILE, DEFAULT_PATH);
        this.queue = Queues.newLinkedBlockingQueue(config.getConfigInt(Configuration.EXPORT_QUEUE_SIZE, DEFAULT_QUEUE_SIZE));
        performExport();
    }

    private void performExport() {
        Executors.newSingleThreadExecutor().execute(() -> {
            try {
                Path path = Paths.get(filePath);
                Trade rejectedTrade = null;
                while ((rejectedTrade = queue.take()) != null) {
                    List<Trade> exportedTrades = Lists.newArrayList(rejectedTrade);
                    queue.drainTo(exportedTrades);
                    Files.write(path, exportedTrades.stream().map(Trade::toString).collect(Collectors.toList()), StandardCharsets.UTF_8, StandardOpenOption.APPEND, StandardOpenOption.CREATE);
                }
            }
            catch (IOException ioe) {
                log.error("Error exporting rejected trades", ioe);
            }
            catch (Exception e) {
                log.error("Error exporting rejected trades", e);
            }
        });
    }

    public void exportTrades(List<Trade> rejectedTrades) {
        rejectedTrades.stream().forEach(t -> {
            try {
                queue.put(t);
            }
            catch (InterruptedException e) {
                log.error("Error putting rejected trade to export queue:" + t, e);
            }
        });

    }

}
