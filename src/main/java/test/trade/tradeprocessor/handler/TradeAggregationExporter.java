package test.trade.tradeprocessor.handler;

import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.extern.log4j.Log4j;
import test.trade.tradeprocessor.configuration.Configuration;
import test.trade.tradeprocessor.data.AggregatedTrade;
import test.trade.tradeprocessor.data.Trade;
import test.trade.tradeprocessor.module.TradeProcessorModule;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * We use a single thread to write to the file as using multiple thread to write to different file
 * offset generally suffer from worse IO performance due to multiple disk seek. We use a blocking
 * queue to buffer the write and the write thread will keep pulling from that queue
 */
@Singleton
@Log4j
public class TradeAggregationExporter {

    private static final int DEFAULT_QUEUE_SIZE = 1000;

    private final String DEFAULT_PATH = "aggregated.txt";

    private final String filePath;
    private final BlockingQueue<AggregatedTrade> queue;

    @Inject
    public TradeAggregationExporter(Configuration config) {
        this.filePath = config.getConfigString(Configuration.ERROR_FILE, DEFAULT_PATH);
        this.queue = Queues.newLinkedBlockingQueue(config.getConfigInt(Configuration.EXPORT_QUEUE_SIZE, DEFAULT_QUEUE_SIZE));
        performExport();
    }

    private void performExport() {
        Executors.newSingleThreadExecutor().execute(() -> {
            try {
                Path path = Paths.get(filePath);
                AggregatedTrade aggregatedTrade = null;
                while ((aggregatedTrade = queue.take()) != null) {
                    List<AggregatedTrade> exportedTrades = Lists.newArrayList(aggregatedTrade);
                    queue.drainTo(exportedTrades);
                    Files.write(path, exportedTrades.stream().map(AggregatedTrade::toString).collect(Collectors.toList()), StandardCharsets.UTF_8, StandardOpenOption.APPEND, StandardOpenOption.CREATE);
                }
            }
            catch (IOException ioe) {
                log.error("Error exporting aggregated trades", ioe);
            }
            catch (Exception e) {
                log.error("Error exporting aggregated trades", e);
            }
        });
    }

    public void exportTrade(AggregatedTrade aggregatedTrade) {
        try {
            queue.put(aggregatedTrade);
        }
        catch (InterruptedException e) {
            log.error("Error putting aggregated trade to export queue:" + aggregatedTrade, e);
        }
    }
}
